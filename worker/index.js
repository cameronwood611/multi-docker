const redis = require("redis");
const config = require("./config");


const client = redis.createClient({
    host: config.redisHost,
    port: config.redisPort,
    retry_strategy: () => 1000
});

const subClient = client.duplicate();

function fib(index) {
    if (index <= 1) return 1;
    return fib(index - 1) + fib(index - 2);
}

subClient.on('message', (channel, msg) => {
    client.hset('values', msg, fib(parseInt(msg)));
});
subClient.subscribe('insert');
const config = require("./config");
const pg = require("pg");
const redis = require("redis");

const express = require("express");
const cors = require("cors");

const app = express();
app.use(cors());
app.use(express.json());

const pgClient = new pg.Pool({
    user: config.pgUser,
    host: config.pgHost,
    database: config.pgDb,
    password: config.pgPass,
    port: config.pgPort
});
pgClient.on("connect", (client) => {
    client
      .query("CREATE TABLE IF NOT EXISTS values (number INT)")
      .catch((err) => console.error(err));
  });

const redisClient = redis.createClient({
    host: config.redisHost,
    port: config.redisPort,
    retry_strategy: () => 1000
});

const redisPublisher = redisClient.duplicate();

app.get('/', (req, res) => {
    res.send("Hello!");
});

app.get('/values/all', async (req, res) => {
    const values = await pgClient.query('SELECT * FROM values');

    res.send(values.rows);
});

app.get('/values/current', async (req, res) => {
    redisClient.hgetall('values', (err, vals) => {
        res.send(vals);
    });
});

app.post('/values', async (req, res) => {
    const index = req.body.index;

    if (parseInt(index) > 40) {
        return res.status(422).send("Index too high.");
    }

    redisClient.hset('values', index, 'TBD');
    redisPublisher.publish('insert', index);
    pgClient.query('INSERT INTO values(number) VALUES $1', [index]);

    res.send({ working: true });
});

app.listen(5000, err => {
    console.log('Listening');
})
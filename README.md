# Complex Fibonacci Calculator

## TECH

- A reactJS front-end with express endpoints are set up.
- There is an nginx server routing their respective files.
- A javascript service is listening for redis database entires.
- Then, data is persisted inside a PSQL database.
- All of this runs inside multiple Docker containers, continuosly integrated (with Travis CI), and deployed on AWS.

### AWS Services
- Elastic Beanstalk and Elastic Container Service (ECS) were used to deploy containers and service the application (mainly nginx, express, react and the javascript worker).
- Elastic cache was used for the redis instance.
- Relational Database Service (RDS) was used to persist the Postgres database.


## RUN
- This was deployed to AWS but to test locally, please run ```docker-compose up --build```.